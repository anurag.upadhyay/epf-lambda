const {
    getTimeDiffInMinute,
    runCommand,
    isNullOrEmpty,
    promiseTimeOut,
    evaluateCaptchaFromImage,
    getCompanyInfo,
} = require("./helpers");
const { v4: uuidv4 } = require('uuid');
const { writeFile } = require("fs-extra");
const { Chrome } = require("./chrome");
const { HTTP_STATUS_CODE } = require("./enums");
const { networkConditions } = require("puppeteer-core");


const epfLoginPageUrl = "https://passbook.epfindia.gov.in/MemberPassBook/Login";
const epfRedirectPageUrl = "https://passbook.epfindia.gov.in/MemberPassBook/passbook";

class EpfService {
    constructor(event, pageService) {
        this.event = event;
        this.pageService = pageService;
    }

    // @ExpectBody({ uan: "", password: "", hashedPassword: "", auto: true, afterDate: "" })
    async index() {
        // const eventBody = JSON.parse(this.event.body);
        // const eventBody = this.event.body;
        let eventBody;
        try{
            eventBody = JSON.parse(this.event.Records[0].body);
            console.log("LOG :: String converted to JSON");
        }
        catch (e){
            eventBody = this.event.Records[0].body;
            console.log("LOG :: JSON recived");
        }
        this.body = {
            uan: eventBody.uan,
            password: eventBody.password,
            hashedPassword: eventBody.hashedPassword,
            auto: eventBody.auto,
            afterDate: eventBody.afterDate,
            captchaText: "",
        };

        const id = uuidv4();
        console.log("INFO :: request accepted with uan", this.body.uan, "pageid is", id);
        // console.log(`page count ${this.pageService.getPagesCount()}`)
        // if (this.pageService.getPagesCount() > 0 || (await this.pageService.getTabsCount()) > 1) {
        //     console.log("processing another request");
        //     return {body: {
        //         success: false,
        //         message: "Processing another request"         // thing to remove
        //     }, 
        // statusCode: 503);
        // }
        let shouldParseCaptcha = this.body.auto;
        if (shouldParseCaptcha == null) {
            shouldParseCaptcha = true;
        }

        this.param = {};
        this.param.pageId = id;

        await Chrome.startBrowser();

        const page = await Chrome.browser.newPage();
        await page.setViewport({ width: 1366, height: 768 });
        this.pageService.addPage(id, page, this.body);
        try {
            await page.goto(epfLoginPageUrl);
            // await page.waitForNavigation({
            //     waitUntil: 'networkidle0',
            // });
            // await page.reload({ waitUntil: ["networkidle0", "domcontentloaded"] });
            // await page.evaluate(() => {
            //     location.reload(true);
            // })
        } catch (error) {
            return {
                statusCode: HTTP_STATUS_CODE.Ok,
                body: await this.exceptionResult("navigation timeout occured")
            }
        }
        await page.setUserAgent('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 Safari/537.36');

        const listenToAlert = () => {
            page.on("dialog", async (dialog) => {
                this.pageService.onAlertMessage(dialog, id);
            });
        };
        listenToAlert();
        let imgPath;
        // console.log("capturing captcha");
        try {
            imgPath = await this.captureAndSaveCaptcha(page, id);
        } catch (error) {
            return {
                statusCode: HTTP_STATUS_CODE.Ok,
                body: await this.exceptionResult("Unable to capture and save captcha")
            } 
        }

        console.log("DEBUG :: image captured and path is", imgPath);

        if (shouldParseCaptcha) {
            console.log("parsing captcha");
            try {
                console.log("DEBUG :: ",Chrome.captchaFolder + imgPath);
                this.body.captchaText = await evaluateCaptchaFromImage(
                    Chrome.captchaFolder + imgPath
                );
            } catch (error) {
                console.log(`${error.message} , for page id : ${this.param.pageId}`);
                return {
                    statusCode: HTTP_STATUS_CODE.Ok,
                    body: await this.exceptionResult("Unable to break captcha")
                };
            }
            console.log("captcha parsed successfully");
            // await promiseTimeOut(1);
            return await this.processSignIn();
        }
        await page.close();                        // Close the website     
        console.log("LOG :: Browser closed");
        return {
            statusCode: HTTP_STATUS_CODE.Ok,
            body: JSON.stringify({ pageId: id })
        };
    }

    // @Worker([HTTP_METHOD.Post])
    // @ExpectBody({ captchaText: "" })
    // @Route("/{pageId}/sign-in")
    async processSignIn() {
        const pageId = this.param.pageId;
        console.log('INFO :: processing sign in', 'pageId', pageId);
        const captcha = this.body.captchaText;
        const page = this.pageService.getPage(pageId);
        const actionResult = {
            success: true,
            data: {

            },
            message: null,
            isValidCredential: true
        }
        if (page != null) {
            const epfData = {
                uan: page.uan,
                password: page.password,
                captchaText: captcha,
                hashedPassword: page.hashedPassword
            }
            console.log('INFO :: epfData : ', 'uan :', epfData.uan, 'captcha :', epfData.captchaText);
            const browserPage = page.browserPage;
            return new Promise(async (res, rej) => {
                const logOut = async () => {
                    await browserPage.evaluate(() => {
                        const logOutBtn = document.querySelector('#logout');
                        if (logOutBtn) {
                            logOutBtn.click();
                        }
                    })
                }
                const resolveResult = async (result) => {
                    await logOut();
                    await this.pageService.removePage(pageId);
                    res(result);
                }
                // browserPage.setRequestInterception(true);
                // browserPage.on('request', async req => {
                //     const url = req.url().toLowerCase();
                //     this.logger.debug('url', url, 'post data', req.postData(),
                //         'headers', JSON.stringify(req.headers()));
                //     req.continue();
                // });
                const takeScreenShot = async (step) => {
                    await browserPage.screenshot({ path: `./logs/${pageId}_step${step}.png`, fullPage: true });
                }
                const onSuccess = (data) => {
                    actionResult.data = data;
                    actionResult.success = true;
                    resolveResult({ body: actionResult });
                    console.log(`INFO :: successfully done for uan - ${epfData.uan}`)
                }
                const onFail = (message, statusCode) => {
                    actionResult.message = message;
                    actionResult.success = false;
                    resolveResult({ body: actionResult, statusCode: statusCode });
                    console.log(`INFO :: failed & handled for uan - ${epfData.uan}, message : ${message}`)
                }
                const getEmployeeIdValues = async () => {
                    await promiseTimeOut(1, true);
                    const dropDownElement = await browserPage.$('#selectmid');
                    const members = await browserPage.evaluate((el) => {
                        const values = [];
                        el.querySelectorAll('option').forEach(option => {
                            values.push({
                                text: option.text,
                                value: option.value
                            });
                        });
                        return values;
                    }, dropDownElement);
                    members.shift();
                    return members;
                };
                const initiateFetchTransaction = async () => {
                    try {
                        const members = await getEmployeeIdValues();
                        const data = {
                            members: []
                        }
                        if (members.length === 0) {
                            onSuccess(data);
                            return;
                        }
                        console.log('INFO :: members', members);
                        for (const member of members) {
                            const memberInfo = await this.getMemberInfo(browserPage, member);
                            // if (memberInfo.transactions.length > 0) {
                            data.members.push(
                                memberInfo
                            );
                            // await promiseTimeOut(100, false);
                            // await takeScreenShot(step++);
                            // }
                            if (memberInfo.company.success === false) {
                                console.log(`INFO :: unable to download passbook of user with uan - ${epfData.uan}, memberId - ${memberInfo.memberId}`)
                            }

                        }
                        onSuccess(data);
                    } catch (err) {
                        console.log(`ERROR ::Error occured, message : ${err.message} , 
                        stack : ${err.stack}, name: ${err.name}
                        `);
                        onFail("error occured in initiateFetchTransaction", HTTP_STATUS_CODE.InternalServerError)
                    }
                }
                const responseCallback = async (response) => {

                    const url = response.url().toLowerCase();
                    const statusCode = response.status();
                    if (url.indexOf('checklogin') >= 0) {
                        // this.logger.debug('check login hitted')
                        if (statusCode === HTTP_STATUS_CODE.Ok) {


                            const text = await response.text();
                            // this.logger.debug('text', text);
                            const json = JSON.parse(text);
                            if (json.status === 0) { // successfully login done
                                const errorMessage = page.errorMessage;
                                if (errorMessage != null) {
                                    onFail(errorMessage, HTTP_STATUS_CODE.BadRequest);
                                }
                                else {
                                    console.log('INFO :: login successful');
                                    await browserPage.waitForNavigation({ waitUntil: 'networkidle0', timeout: 0 });
                                    initiateFetchTransaction();
                                }
                            }
                            else { // error message
                                console.log('INFO :: invalid form data', json);
                                if (json.message.match(/password/i)) {
                                    actionResult.isValidCredential = false;
                                    onFail(json.message, HTTP_STATUS_CODE.Ok);
                                }
                                else {
                                    onFail(json.message, HTTP_STATUS_CODE.BadRequest);
                                }

                            }
                        }
                        else {
                            onFail(`Unable to login`, HTTP_STATUS_CODE.InternalServerError)
                        }
                        // await browserPage.waitForNavigation({
                        //     waitUntil: 'networkidle0',
                        // });

                        browserPage.off('response', responseCallback);
                    }


                };
                browserPage.on('response', responseCallback);
                //fill data
                // let step = 0;
                // await takeScreenShot(step++);
                await browserPage.evaluate((data) => {
                    document.querySelector('#username').focus();
                    document.querySelector('#username').value = data.uan;
                }, epfData);
                // await promiseTimeOut(100, false);
                // await takeScreenShot(step++);
                await browserPage.evaluate((data) => {
                    document.querySelector('#password').focus();
                    document.querySelector('#password').value = data.password;
                }, epfData);
                // await promiseTimeOut(100, false);
                // await takeScreenShot(step++);
                await browserPage.evaluate((data) => {
                    document.querySelector('#captcha').focus();
                    document.querySelector('#captcha').value = data.captchaText;
                    //document.querySelector('#login').focus();
                }, epfData);
                // await promiseTimeOut(1);
                // await takeScreenShot(step++);
                const isBtnExist = await browserPage.evaluate(() => {
                    const btnLogin = document.querySelector('#login');
                    return btnLogin != null;
                });
                if (isBtnExist === true) {
                    if (isNullOrEmpty(epfData.hashedPassword)) {
                        await browserPage.evaluate(() => {
                            document.querySelector('#login').click();
                        });
                        // await promiseTimeOut(1);
                        // await takeScreenShot(step++);
                        console.log("INFO :: login button clicked");
                        const errorMessage = page.errorMessage;
                        if (errorMessage != null) {
                            if (errorMessage.match(/valid uan/i)) {
                                actionResult.isValidCredential = false;
                                onFail(errorMessage, HTTP_STATUS_CODE.Ok);
                            }
                            else {
                                onFail(errorMessage, HTTP_STATUS_CODE.BadRequest);
                            }
                        }
                    }
                    else {
                        await browserPage.evaluate((epfData) => {
                            const data = {
                                username: epfData.uan,
                                password: epfData.hashedPassword,
                                captcha: epfData.captchaText
                            }
                            var url = '/MemberPassBook/passbook/api/ajax/checkLogin';
                            $.ajax({
                                type: "POST",
                                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                                url: url,
                                data: data,
                                dataType: 'json',
                                timeout: 100000,
                                success: function (data) {
                                    // $this.button('reset');
                                    var result = data;

                                    if (result.status !== undefined) {
                                        if (result.status === 0) {
                                            setTimeout(page_Redirect, 50);
                                        }
                                        if (result.status === 1) {
                                            // $this.button('reset');
                                            var err = "<div class='alert alert-danger alert-dismissible show' role='alert'><strong>" + result.message + " </strong><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
                                            $('#div_page_msg').html(err);
                                            getcap();
                                        }
                                    } else {
                                        // $this.button('reset');
                                        var err = "<div class='alert alert-danger alert-dismissible show' role='alert'><strong> Invalid output </strong><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
                                        $('#div_page_msg').html(err);
                                        getcap();
                                    }
                                },
                                error: function (x, t, m) {
                                    // $this.button('reset');
                                    if (t === "timeout") {
                                    } else {
                                        if (m.length > 0) {
                                            var err = "<div class='alert alert-danger alert-dismissible show' role='alert'><strong> " + m + " </strong><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>";
                                            $('#div_page_msg').html(err);
                                            getcap();
                                        }
                                    }
                                }
                            });
                        }, epfData)
                    }



                }
                else {
                    onFail("EPF site is in maintainance mode", 503)
                }
            })

        }
        return textResult("Bad pageId", HTTP_STATUS_CODE.BadRequest);
    }

    async getMemberInfo(page, member, company) {
        console.log(
            "member in member info",
            member,
            "afterDate",
            this.body.afterDate
        );
        const result = {
            memberId: member.text,
            company,
            transactions: [],
        };
        await promiseTimeOut(5, true);
        await page.select("#selectmid", member.value);
        await page.select("#selectmid", member.value);

        await promiseTimeOut(5, true);

        const memberPassbookresult = await this.getMemberPassbook(page);
        // console.log("memberPassbookresult", memberPassbookresult)
        if (memberPassbookresult && memberPassbookresult.success === false) {
            result.company = {
                success: false,
                errorMessage: memberPassbookresult.message,
            };
            return result;
        }

        await promiseTimeOut(10, true);

        if (company == null) {
            try {
                await new Promise(
                    async (onCompanyDownloadSuccess, onCompanyDownloadFailed) => {
                        // let isDownloadPassbookAvailable = false
                        // let onCompanyDownloadSuccess;
                        // let onCompanyDownloadFailed;
                        const responseCallback = async (response) => {
                            const url = response.url().toLowerCase();
                            const statusCode = response.status();
                            if (
                                url.indexOf("download-member-passbook") >= 0 &&
                                statusCode === HTTP_STATUS_CODE.Ok
                            ) {
                                const text = await response.text();
                                // console.log('text', text);
                                const json = JSON.parse(text);
                                // console.log('text from download api is', text)
                                if (json[0].download) {
                                    await promiseTimeOut(5, true);
                                    onCompanyDownloadSuccess();
                                } else {
                                    // console.log('download not available')
                                    onCompanyDownloadFailed(new Error(json[0].message));
                                }
                                page.off("response", responseCallback);
                                const downloadModal = await page.$("#btnCloseModel");

                                if (downloadModal) {
                                    await downloadModal.click();
                                    console.log("closed download modal");
                                }
                            }
                        };
                        page.on("response", responseCallback);
                        // download report
                        await page._client.send("Page.setDownloadBehavior", {
                            behavior: "allow",
                            downloadPath: `${Chrome.captchaFolder}`,
                        });
                        let downloadPassBookBtn = await page.$("#btnDownloadPassbookOld");
                        const clickOnDownloadPassBook = () => {
                            console.log("download passbook click");
                            // await page.click("#btnDownloadPassbookOld");
                            return downloadPassBookBtn.click();
                        };
                        if (downloadPassBookBtn) {
                            await clickOnDownloadPassBook();
                        } else {
                            console.log("waiting for download passbook btn");
                            await promiseTimeOut(10, true);
                            console.log("wait finished for download passbook btn");

                            downloadPassBookBtn = await page.$("#btnDownloadPassbookOld");
                            await clickOnDownloadPassBook();
                        }

                        // await promiseTimeOut(2);

                        // onCompanyDownloadSuccess = res;
                        // onCompanyDownloadFailed = rej;
                    }
                );
            } catch (ex) {
                // console.log('download not available, catch handled')
                result.company = {
                    success: false,
                    errorMessage: ex.message,
                };
                return result;
            }

            console.log("processing download of member passbook");
            await page.evaluate(() => {
                // debugger;
                document
                    .querySelector("#downloadTable tbody tr")
                    .cells[2].querySelector("button")
                    .click();
            });
            await promiseTimeOut(1, true);
            try {
                company = await getCompanyInfo(`${Chrome.captchaFolder}${member.text}`);
                result.company = company;
            } catch (error) {
                result.company = {
                    success: false,
                };
                return result;
            }
            console.log("company info extracted");
        }
        await promiseTimeOut(1, true);
        const clickOnNextPage = async () => {
            return await page.evaluate((memberText) => {
                const nextBtn = document.querySelector(`#tbl_${memberText}_next`);
                const isDisabled = nextBtn.classList.contains("disabled");
                if (!isDisabled) {
                    nextBtn.click();
                }
                return !isDisabled;
            }, member.text);
        };
        const getTableData = async () => {
            return await page.evaluate(
                (data) => {
                    var values = [];
                    const isValueZero = (val) => {
                        return val == "0" || val.length === 0;
                    };
                    const getAmount = (val) => {
                        return Number(val.replace(/,/g, ""));
                    };
                    const getTxType = (val, particulars) => {
                        if (isValueZero(val)) {
                            if (particulars.match(/int/i)) {
                                return "interest";
                            }
                            return "contribution";
                        }
                        return "withdrawl";
                    };
                    const lastDate =
                        data.afterDate == null || data.afterDate.length === 0
                            ? new Date("1500-10-10")
                            : new Date(data.afterDate);
                    // const lastMonth = lastDate.getMonth();
                    const monthDiff = (dateFrom, dateTo) => {
                        return (
                            dateTo.getMonth() -
                            dateFrom.getMonth() +
                            12 * (dateTo.getFullYear() - dateFrom.getFullYear())
                        );
                    };

                    const rows = document.querySelectorAll(
                        `#tbl_${data.member.text} tbody tr`
                    );
                    for (var i = 0, length = rows.length; i < length; i++) {
                        const row = document.querySelectorAll(
                            `#tbl_${data.member.text} tbody tr`
                        )[i];
                        const txnDateString = row.cells[0].innerText;
                        const txnDate = new Date(txnDateString);
                        //debugger;
                        if (monthDiff(lastDate, txnDate) <= 0) {
                            break;
                        }
                        const particulars = row.cells[2].innerText;
                        const withdrawlEmployeeVal = row.cells[5].innerText;
                        const withdrawlEmployerVal = row.cells[6].innerText;
                        values.push({
                            txnDate: txnDateString,
                            txnType: getTxType(withdrawlEmployeeVal, particulars),
                            particulars: particulars,
                            contribution: {
                                employee: getAmount(
                                    isValueZero(withdrawlEmployeeVal)
                                        ? row.cells[3].innerText
                                        : withdrawlEmployeeVal
                                ),
                                employer: getAmount(
                                    isValueZero(withdrawlEmployerVal)
                                        ? row.cells[4].innerText
                                        : withdrawlEmployerVal
                                ),
                            },
                        });
                    }

                    return values;
                },
                {
                    member,
                    //index,
                    afterDate: this.body.afterDate,
                }
            );
        };
        let isNextBtnActive = true;
        while (isNextBtnActive === true) {
            // await clickOnTablePage(index);
            const tx = await getTableData();
            result.transactions = [...result.transactions, ...tx];
            await promiseTimeOut(200, false);
            isNextBtnActive = await clickOnNextPage();
            console.log("clicking on next table page");
        }
        return result;
    }

    async badRequestMessage(msg) {
        console.log("bad request", msg);
        await this.pageService.removePage(this.param.pageId);
        return {
            message: {
                success: false,
                message: msg,
            },
            status: HTTP_STATUS_CODE.BadRequest,
        };
    }

    async exceptionResult(errMessage) {
        console.log("exception", errMessage);
        await this.pageService.removePage(this.param.pageId);
        return {
            status: 500,
            success: false,
            message: errMessage || "Server seems to be busy, please try again later.",
        };
    }

    captureAndSaveCaptcha(page, id) {
        let retry_count = 0;
        const maxRetryCount = 10; // 5 second
        console.log(' retry_count', retry_count);
        return new Promise((res, rej) => {
            console.log('inside promise');
            const checkAndCaptureImageRecursiuvely = async () => {
                await page.waitForSelector("#captcha_id");
                const captchaImg = await page.$("#captcha_id");
                // console.log("captchaImg", captchaImg);
                if (captchaImg != null) {
                    const imgFolder = Chrome.captchaFolder;
                    console.log('imgFolder: ',imgFolder);
                    const imgPath = `${id}.png`;
                    console.log('taking screenshot of img');
                    const imgBuffer = await captchaImg.screenshot({ path: `${imgFolder}${imgPath}`, }); //{path: `${imgFolder}${imgPath}`,}
                    // console.log("imgBuffer",imgBuffer);
                    // const result = await writeFile(`${imgFolder}${imgPath}`, imgBuffer, 'binary');  //push to s3
                    console.log('result placed');
                    res(imgPath);
                } else {
                    console.log("executing else block", retry_count);
                    if (retry_count++ <= maxRetryCount) {
                        console.log("retry_count", retry_count);
                        setTimeout(checkAndCaptureImageRecursiuvely, 500);
                    } else {
                        console.log(`ERROR :: unable to retrieve image`);
                        rej("unable to retrieve image");
                    }
                }
            };
            checkAndCaptureImageRecursiuvely();
        });
    }

    // @Worker()
    // @Route('/status.json')
    async getStatus() {
        if (this.pageService.getPagesCount() > 0) {
            return { message: "processing", status: HTTP_STATUS_CODE.BadRequest };
        }
        return { message: "free" };
    }

    async getMemberPassbook(browserPage) {
        return new Promise(async (res, rej) => {
            try {
                await browserPage.click("#btnPassbookOld");
                const responseCallback = async (response) => {
                    const url = response.url().toLowerCase();
                    const statusCode = response.status();
                    if (url.indexOf("get-member-passbook") >= 0) {
                        if (statusCode === HTTP_STATUS_CODE.Ok) {
                            const text = await response.text();
                            // console.log('text', text);
                            const json = JSON.parse(text);
                            // console.log('text from get-member-passbook api is', json)
                            if (json[0].html) {
                                console.log("get-member-passbook resolved");
                                res();
                            } else {
                                console.log("DEBUG :: download not available");
                                res({
                                    message: json[0].message,
                                    success: false,
                                });
                            }
                        } else {
                            rej(
                                `Unable to view member passbook, statusCode: ${statusCode}, method: getMemberPassbook`
                            );
                        }
                        browserPage.off("response", responseCallback);
                    }
                };
                browserPage.on("response", responseCallback);
                await promiseTimeOut(5, true);
                await browserPage.click("#btnPassbookOld");
                console.log("click on view old passbook");
            } catch (ex) {
                rej(ex);
            }
        });
    }
}


module.exports={
    EpfService
}