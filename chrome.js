const chrome = require("chrome-aws-lambda");
const path = require("path");
let browser;

class Chrome {
    static async startBrowser() {
        browser = await chrome.puppeteer.launch({
            executablePath: await chrome.executablePath,
            defaultViewport: chrome.defaultViewport,
            headless: true, //chrome.headless, 
            args: chrome.args,
            ignoreHTTPSErrors: true
        });
    }

    static get browser() {
        return browser;
    }

    static get timeToCheckPageClearInMinute() {
        return 30;
    }

    static get expiredTimeInMinute() {
        return 15;
    }

    static get captchaFolder() {
        return path.join("/tmp/");
    }

    static lastTimeOfPageClearRun = new Date();
}


module.exports = {
    Chrome
}