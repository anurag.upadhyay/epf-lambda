const { PAGE_STATE } = require("./enums");
const { Chrome } = require("./chrome");
const { getTimeDiffInMinute } = require("./helpers");

const pages = {};

class EpfPageService {
    addPage (id, page, payload) {
        pages[id] = {
            browserPage: page,
            state: PAGE_STATE.Idle,
            addedTime: new Date(),
            uan: payload.uan,
            password: payload.password,
            hashedPassword: payload.hashedPassword
        };
    }

    markPageBusy (id) {
        this.changePageState(id, PAGE_STATE.Busy);
    }

    changePageState (id, state) {
        if (pages[id]) {
            pages[id].state = state;
        }
    }

    markPageCompleted (email) {
        for (var pageId in pages) {
            if (pages[pageId].email === email) {
                this.changePageState(pageId, PAGE_STATE.Done);
            }
        }
    }

    isUanProccessed (email) {
        for (var pageId in pages) {
            const page = pages[pageId];
            if (page.uan === uan && page.state === PAGE_STATE.Done) {
                return true;
            }
        }
        return false;
    }

    getPage (id) {
        return pages[id];
    }

    async onAlertMessage (dialog, id) {
        if (pages[id]) {
            const msg = dialog.message();
            pages[id].errorMessage = msg;
        }
        await dialog.dismiss();
    }

    resetErrorMessage (id) {
        if (pages[id]) {
            pages[id].errorMessage = null;
        }
    }

    markPageReqIntercepted (id) {
        if (pages[id]) {
            pages[id].reqIntercepted = true;
        }
    }

    async removeAllPage (id) {
          Promise.all(
            Object.keys(pages).map(async (id) => {
                await pages[id].browserPage.close();
                delete pages[id];
            })
        )

        // Promise.all(
        //     await Chrome.browser
        // )
        // if (pages[id]) {
        //     await pages[id].browserPage.close();
        //     delete pages[id];
        //     // try {
        //     //     await remove(`${Chrome.captchaFolder}${id}.png`);
        //     // } catch (error) { }
        // }
    }

    async removePage (id) {
        if (pages[id]) {
            await pages[id].browserPage.close();
            delete pages[id];
            // try {
            //     await remove(`${Chrome.captchaFolder}${id}.png`);
            // } catch (error) { }
        }
    }

    async checkForExpiredPage () {
        const now = new Date();
        try {
            for (var pageId in pages) {
                const minutes = getTimeDiffInMinute(now, pages[pageId].addedTime);
                if (minutes > Chrome.expiredTimeInMinute) {
                    await this.removePage(pageId);
                }
            }
            Chrome.lastTimeOfPageClearRun = now;
        } catch (ex) {
            console.log(
                `error occured in checkForExpiredPage message: ${ex.message}, stack : ${ex.stack}`
            );
        }
    }

    getPages () {
        return pages;
    }

    getPagesCount () {
        return Object.keys(pages).length;
    }

    async   getTabsCount () {
        return (await Chrome.browser.pages()).length
    }
}


module.exports = {
    EpfPageService
}