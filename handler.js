
const Sentry = require("@sentry/serverless");
const { EpfPageService }  = require("./epf_page_service");
const { EpfService } = require("./epf_service");



module.exports.epf = Sentry.AWSLambda.wrapHandler(async (event) => {
  const pageService= new EpfPageService();
  const service = new EpfService(event, pageService);
  return await service.index();
});