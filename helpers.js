const { exec } = require("child_process");
// const { createWorker } = require('tesseract.js');
const { createReadStream, exists } = require('fs-extra');
const path = require("path");
const { getTextFromImage, isSupportedFile } = require('@shelf/aws-lambda-tesseract');

module.exports.handler = async event => {
    // assuming there is a photo.jpg inside /tmp dir
    // original file will be deleted afterwards

    if (!isSupportedFile('/tmp/photo.jpg')) {
        return false;
    }

    return getTextFromImage('/tmp/photo.jpg');
};



const readline = require('readline');
const companyIdentificationString = 'Establishment ID/Name';
const companyIdentificationStringLength = companyIdentificationString.length;

function getTimeDiffInMinute(time1, time2) {
    var diff = time1 - time2;
    return Math.floor(diff / 60000);
};

function runCommand(cmd) {
    return new Promise(function (res, rej) {
        let cmdOutput = "";
        var command = exec(cmd);
        command.on("error", rej);
        command.stdout.on('data', function (data) {
            // console.log('LOG :: data', data.toString());
            cmdOutput += data.toString();
        });
        if (process.env.NODE_ENV != 'production') {
            command.stderr.on('data', function (data) {
                console.log("ERROR :: ", data.toString());
            });
        }
        command.on('exit', function (code) {
            // console.log('LOG :: cmdOutput', cmdOutput);
            res(cmdOutput);
        });
    });
}

function isNullOrEmpty(value) {
    return value == null || value.length === 0;
}

function promiseTimeOut(time, isTimeInSecond = true) {
    return new Promise((res) => {
        setTimeout(res, time * isTimeInSecond ? 1000 : 1);
    });
}

async function evaluateCaptchaFromImage(imgPath) {
    // console.log("yaha hu mai ", path.join("lang-data"));
    // const worker = createWorker({
    //     langPath: path.join("lang-data"), 
    //     logger: m => console.log("LOG :: ",m)
    //   });
    // console.log("fir yaha pe");
    // await worker.load();
    // await worker.loadLanguage('eng');
    // await worker.initialize('eng');
    // let { data: { text } } = await worker.recognize(imgPath);
    // // console.log(text);
    // await worker.terminate();
    // // let text = await runCommand(`tesseract ${imgPath} stdout`);
    if (!isSupportedFile(imgPath)) {
        return false;
    }
    let text = await getTextFromImage(imgPath);
    console.log("LOG :: parsed text from ocr is", text);
    text = evaluateExpressionFromText(text);
    console.log(`LOG :: evaluated value is: '${text}'`);
    return String(text);
}

// function evaluateExpressionFromText (text) {
//     // remove = symbol
//     // console.log('text parsed from', text);
//     text = text.replace(/=/g, '').trim().replace(/ /g, '');
//     // logger.log('expression', `'${text}'`, 'valid', /^[0-9+-/*//]*$/.test(text));
//     console.log("evaluateExpressionFromText",text);
//     if (/^[0-9+-/*//]*$/.test(text)) {
//         // logger.debug('evaluating', text, eval(text))
//         const lastChar = text[text.length - 1];
//         if (isNaN(Number(lastChar))) {
//             text = text.substring(0, text.length - 1);
//         }
//         return eval(text);
//     }
//     else {
//         throw new Error(`unable to break captcha ${text}`);
//     }
// }

function evaluateExpressionFromText(text) {
    text = text.replace(/=/g, '').trim().replace(/ /g, '');
    // console.log("space trimed",text);
    const lastChar = text.charAt(text.length - 1);
    if (isNaN(Number(lastChar))) {
        text = text.substring(0, text.length - 1);
    }
    // console.log("Going for evaluation",text);
    try {
        text = eval(text);
        // console.log("Evaluated: ",text);
        return text;
    } catch (error) {
        console.log("ERROR ::", error);
    }
}

async function getCompanyInfo(filePath) {
    // console.log('file path', filePath);
    if (! await exists(`${filePath}.pdf`)) {
        console.log("ERROR ::file not found")
        throw new Error(`file ${filePath}.pdf does not exist`)
    }

    await runCommand(`pdftotext -layout ${filePath}.pdf`);  //xpdf
    // pdfUtil.pdfToText(`${filePath}.pdf`, function(err, data) {
    //     if (err) throw(err);
    //     console.log(data); //print text    
    //   });
    // console.log('text extracted');

    const readerStream = createReadStream(`${filePath}.txt`);
    const readInterface = readline.createInterface({
        input: readerStream,
        // output: process.stdout,
        console: false
    });

    return new Promise((res, rej) => {
        readInterface.on('line', (data) => {
            let index = data.indexOf(companyIdentificationString);
            if (index >= 0) {
                readInterface.close();
                readerStream.destroy();
                // index = data.indexOf('ID/Name');
                const companyData = data.substr(index + companyIdentificationStringLength).split("/");
                const company = {
                    id: companyData.shift().trim(),
                    name: companyData.join("/").trim(),
                    success: true
                };
                res(company);
            }
            // console.log('data', data);
        })
    });
}



module.exports = {
    getTimeDiffInMinute,
    runCommand,
    isNullOrEmpty,
    promiseTimeOut,
    evaluateCaptchaFromImage,
    getCompanyInfo,
}