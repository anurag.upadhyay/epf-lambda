const ERROR_TYPE = {
    Captcha_Verification_Failed: "captcha_verification_failed"
}
const PAGE_STATE = {
    Idle: 0,
    Busy: 1,
    Done: 2
}
const HTTP_STATUS_CODE = {
    BadRequest: 400,
    Unauthorized: 401,
    Forbidden: 403,
    NotFound: 404,
    Ok: 200,
    Created: 201,
    NoContent: 204,
    Redirect: 302,
    NotModified: 304,
    MethodNotAllowed: 405,
    NotAcceptable: 406,
    InternalServerError: 500
}

module.exports={
    ERROR_TYPE,
    PAGE_STATE,
    HTTP_STATUS_CODE
}